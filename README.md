# holiday_card_atari8bit

A Simple Holiday Card written using [FastBasic](https://github.com/dmsc/fastbasic) on the Atari 8bit computer.

Released under a GPLv3 License.
